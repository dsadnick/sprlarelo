<update-profile-details :user="user" inline-template>
    <div class="panel panel-default">
        <div class="panel-heading">Profile Details</div>

        <div class="panel-body">
            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                Your profile has been updated!
            </div>

            <form class="form-horizontal" role="form">
                <!-- coach -->
                <div class="form-group" :class="{'has-error': form.errors.has('coach')}">
                    <label class="col-md-4 control-label">Be a Coach?</label>

                    <div class="col-md-6">
                        <input type="checkbox" class="form-control" name="coach" v-model="form.coach">

                        <span class="help-block" v-show="form.errors.has('coach')">
                            @{{ form.errors.get('coach') }}
                        </span>
                    </div>
                </div>

                <!-- steam -->
                <div class="form-group" :class="{'has-error': form.errors.has('steam')}">
                    <label class="col-md-4 control-label">Steam ID</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="steam" v-model="form.steam">

                        <span class="help-block" v-show="form.errors.has('steam')">
                            @{{ form.errors.get('steam') }}
                        </span>
                    </div>
                </div>

                <!-- twitch -->
                <div class="form-group" :class="{'has-error': form.errors.has('twitch')}">
                    <label class="col-md-4 control-label">Twitch ID</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="twitch" v-model="form.twitch">

                        <span class="help-block" v-show="form.errors.has('twitch')">
                            @{{ form.errors.get('twitch') }}
                        </span>
                    </div>
                </div>

                <!-- youtube -->
                <div class="form-group" :class="{'has-error': form.errors.has('youtube')}">
                    <label class="col-md-4 control-label">Youtube ID</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="youtube" v-model="form.youtube">

                        <span class="help-block" v-show="form.errors.has('youtube')">
                            @{{ form.errors.get('youtube') }}
                        </span>
                    </div>
                </div>

                <!-- twitter -->
                <div class="form-group" :class="{'has-error': form.errors.has('twitter')}">
                    <label class="col-md-4 control-label">Twitter ID</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="twitter" v-model="form.twitter">

                        <span class="help-block" v-show="form.errors.has('twitter')">
                            @{{ form.errors.get('twitter') }}
                        </span>
                    </div>
                </div>


                <!-- bio -->
                <div class="form-group" :class="{'has-error': form.errors.has('bio')}">
                    <label class="col-md-4 control-label">Your Bio</label>

                    <div class="col-md-6">
                        <textarea name="bio" v-model="form.bio" cols="50" rows="10"></textarea>
                        <span class="help-block" v-show="form.errors.has('bio')">
                            @{{ form.errors.get('bio') }}
                        </span>
                    </div>
                </div>


                <!-- Update Button -->
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6">
                        <button type="submit" class="btn btn-primary"
                                @click.prevent="update"
                                :disabled="form.busy">

                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</update-profile-details>