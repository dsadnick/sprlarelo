@extends('layouts.main')

@section('breadcrumbs')
    <h1>
        Lessons
        <small> Make a new lesson</small>
    </h1>
@stop

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open([
        'url' => 'lessons'
    ]) !!}

    <div class="form-group">
        {!! Form::label('game', 'Game:', ['class' => 'control-label']) !!}
        {!! Form::select('game', $gamesArray,null,['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <div class='input-group date' id='datetimepicker1'>
            {!! Form::label('when', 'When Date:', ['class' => 'control-label']) !!}
            {!! Form::text('when', '',['class' => 'form-control']) !!}

                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('description', 'What are you looking to learn?', ['class' => 'control-label']) !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Create a new lesson Request', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                calendarWeeks: true,
                showTodayButton: true,
                format: 'YYYY-MM-DD hh:mm:ss',
            });
        });
    </script>


@stop

@section('scripts')
    {!! Html::script('js/moment-develop/moment.js') !!}

@stop