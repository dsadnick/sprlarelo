<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileDetailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update the user's profile details.
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'coach'   => '',
            'steam'   => '',
            'twitch'  => '',
            'youtube' => '',
            'twitter' => '',
            'bio'     => ''
        ]);

        $request->user()->forceFill([
            'coach'   => $request->coach,
            'steam'   => $request->steam,
            'twitch'  => $request->twitch,
            'youtube' => $request->youtube,
            'twitter' => $request->twitter,
            'bio'     => $request->bio
        ])->save();
    }
}