@extends('layouts.main')

@section('breadcrumbs')
    <h1>
        Dashboard
        <small>Your Mission Control</small>
    </h1>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner text-center">
                    <h3>{{$lessonCount}}</h3>

                    <p>Open Lesson Requests</p>
                </div>
                <div class="icon">
                    <i class="fa fa-list"></i>
                </div>
                <a href="/openlessons" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner text-center">
                    <h3>{{$lessonAcceptedCount}}</h3>

                    <p>My Accepted Lessons</p>
                </div>
                <div class="icon">
                    <i class="fa fa-check"></i>
                </div>
                <a href="/openlessons" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner  text-center">
                    <h3>{{$expiredLessons}}</h3>

                    <p>Completed Lessons</p>
                </div>
                <div class="icon">
                    <i class="fa fa-magic"></i>
                </div>
                <a href="/openlessons" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <!--<div class="col-lg-3 col-xs-6"
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>65</h3>

                    <p>Unique Visitors</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>-->
        <!-- ./col -->
    </div>






@stop