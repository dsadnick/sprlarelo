@extends('layouts.main')

@section('breadcrumbs')
    <h1>
        Lessons
        <small> My open lessons</small>
    </h1>
@stop

@section('content')

    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="row">
                    <div class="col col-xs-6">
                        <h3 class="panel-title">Open Lessons <small> non accepted lessons</small></h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <table class="table table-striped table-bordered table-list table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Requested On</th>
                            <th class="text-center">Lesson Requested For</th>
                            <th class="text-center">Requested Game</th>
                            <th class="text-center">What Are You Looking To Learn</th>
                            <th class="text-center">Remove Lesson</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($openLessons as $user)
                        <tr>
                            <td class="text-center">{{$user['created_at']}}</td>
                            <td class="text-center">{{$user['lessonDate']}}</td>
                            <td class="text-center">{{$user['lessonGame']}}</td>
                            <td class="text-center">{{$user['lessonLearnWhat']}}</td>
                            <td class="text-center"><a class="btn btn-danger"><em class="fa fa-trash"></em></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
            <div class="panel-footer">
                <div class="row">

                </div>
            </div>
        </div>

    </div>


    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="row">
                    <div class="col col-xs-6">
                        <h3 class="panel-title">Accepted Lessons <small> accepted but non completed lessons</small></h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <table class="table table-striped table-bordered table-list table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Requested On</th>
                        <th class="text-center">Lesson Requested For</th>
                        <th class="text-center">Requested Game</th>
                        <th class="text-center">What Are You Looking To Learn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($acceptedLessons as $al)
                        <tr>
                            <td class="text-center">{{$al['created_at']}}</td>
                            <td class="text-center">{{$al['lessonDate']}}</td>
                            <td class="text-center">{{$al['lessonGame']}}</td>
                            <td class="text-center">{{$al['lessonLearnWhat']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
            <div class="panel-footer">
                <div class="row">

                </div>
            </div>
        </div>

    </div>

    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="row">
                    <div class="col col-xs-6">
                        <h3 class="panel-title">Completed Lessons <small> completed lessons</small></h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <table class="table table-striped table-bordered table-list table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Requested On</th>
                        <th class="text-center">Lesson Requested For</th>
                        <th class="text-center">Requested Game</th>
                        <th class="text-center">What Are You Looking To Learn</th
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($completedLessons as $cl)
                        <tr>
                            <td class="text-center">{{$cl['created_at']}}</td>
                            <td class="text-center">{{$cl['lessonDate']}}</td>
                            <td class="text-center">{{$cl['lessonGame']}}</td>
                            <td class="text-center">{{$cl['lessonLearnWhat']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
            <div class="panel-footer">
                <div class="row">

                </div>
            </div>
        </div>

    </div>

@stop

@section('scripts')
@stop