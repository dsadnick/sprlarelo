<?php

namespace App\Http\Controllers;

use Auth;
use View;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Lessons;

class DashboardController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');

    }


    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        /*$userID = Auth::user()->id;
        $user = User::find($userID);*/
        $lessonCount = Lessons::where('lessonFor',Auth::user()->id)
            ->where('lessonCompleted','=',NULL)
            ->where('lessonAcceptedOn','=',NULL)
            ->where('lessonAcceptedBy','=',NULL)
            ->count();
        $lessonAcceptedCount = Lessons::where('lessonFor',Auth::user()->id)
            ->where('lessonAcceptedOn','!=',NULL)
            ->where('lessonCompleted','=',NULL)
            ->where('lessonCompletedOn','=',NULL)
            ->count();
        $expiredLessons = Lessons::where('lessonFor',Auth::user()->id)
            ->where('lessonCompleted','!=',NULL)
            ->where('lessonAcceptedOn','!=',NULL)
            ->count();

        return View::make('pages.dashboard')->with(array(
            'lessonCount'=>$lessonCount,
            'lessonAcceptedCount'=>$lessonAcceptedCount,
            'expiredLessons'=>$expiredLessons
        ));
    }

    public function OpenLessons() {

        $openLessons      = Lessons::where('lessonFor',Auth::user()->id)
            ->where('lessonCompleted','=',NULL)
            ->where('lessonAcceptedOn','=',NULL)
            ->where('lessonAcceptedBy','=',NULL)
            ->get();
        $acceptedLessons  = Lessons::where('lessonFor',Auth::user()->id)
            ->where('lessonAcceptedOn','!=',NULL)
            ->where('lessonCompleted','=',NULL)
            ->where('lessonCompletedOn','=',NULL)
            ->get();
        $completedLessons = Lessons::where('lessonFor',Auth::user()->id)
            ->where('lessonCompleted','!=',NULL)
            ->where('lessonAcceptedOn','!=',NULL)
            ->get();

        return View::make('pages.openlessons')->with(array(
            'openLessons'=> $openLessons,
            'acceptedLessons'=>$acceptedLessons,
            'completedLessons' => $completedLessons
        ));

    }


}
