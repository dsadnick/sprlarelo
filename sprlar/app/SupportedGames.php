<?php
/**
 * Created by PhpStorm.
 * User: dsadn_000
 * Date: 5/21/2017
 * Time: 5:05 PM
 */

namespace App;


class SupportedGames {

    protected $SupportedGames = array(
        'bg' => 'Player Unknown Battle Grounds',
        'dota2' => 'Dota 2',
        'lol'=> 'League Of Legends',
        'sc2'=> 'Star Craft 2',
        'csgo' => 'Counter Strike:GO',
        'ow' => 'Overwatch',
        'sf' =>   'Street Fighter'
    );


    public function getSupportedGames(){
        return $this->SupportedGames;
    }


}