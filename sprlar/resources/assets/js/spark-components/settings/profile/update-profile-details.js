Vue.component('update-profile-details', {
    props: ['user'],

    data() {
        return {
            form: new SparkForm({
                coach: '',
                steam: '',
                twitch: '',
                youtube: '',
                twitter: '',
                bio: '',
            })
        };
    },

    mounted() {
        this.form.coach   = this.user.coach;
        this.form.steam   = this.user.steam;
        this.form.twitch  = this.user.twitch;
        this.form.youtube = this.user.youtube;
        this.form.twitter = this.user.twitter;
        this.form.bio     = this.user.bio;
    },

    methods: {
        update() {
            Spark.put('/settings/profile/details', this.form)
                .then(response => {
                    Bus.$emit('updateUser');
                });
        }
    }
});