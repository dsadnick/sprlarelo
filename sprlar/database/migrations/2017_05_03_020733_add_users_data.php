<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('firstname');
            $table->string('lastname');
            $table->boolean('coach');
            $table->string('username')->unique();
            $table->string('steam');
            $table->string('twitch');
            $table->string('youtube');
            $table->string('twitter');
            $table->string('bio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('firstname');
            $table->dropColumn('lastname');
            $table->dropColumn('coach');
            $table->dropColumn('username');
            $table->dropColumn('steam');
            $table->dropColumn('twitch');
            $table->dropColumn('youtube');
            $table->dropColumn('twitter');
            $table->dropColumn('bio');
        });
    }
}
